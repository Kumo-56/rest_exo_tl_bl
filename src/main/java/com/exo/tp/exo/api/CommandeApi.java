package com.exo.tp.exo.api;

import com.exo.tp.exo.dto.CommandeDto;
import com.exo.tp.exo.exception.EmptyArticlesException;
import com.exo.tp.exo.exception.UnknownClientException;
import com.exo.tp.exo.mapper.CommandeMapper;
import com.exo.tp.exo.service.CommandeService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api/v1/commandes", produces = APPLICATION_JSON_VALUE)
public class CommandeApi {

    private final CommandeService commandeService;
    private final CommandeMapper commandeMapper;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<CommandeDto>> getAllCommandes() {
        return ResponseEntity.ok(
                this.commandeService.getAllCommandes().stream()
                        .map(this.commandeMapper::mapToDto)
                        .toList()
        );
    }

    @PostMapping(
            consumes = {APPLICATION_JSON_VALUE},
            produces = {APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @ApiResponse(responseCode = "201", description = "Succes")
    public ResponseEntity<CommandeDto> createCommande(@RequestBody final CommandeDto commandeDto) {
        try {
            CommandeDto commandeDtoReponse = this.commandeMapper.mapToDto(
                    this.commandeService.createCommande(
                            this.commandeMapper.mapToModel(commandeDto)
                    )
            );
            return ResponseEntity
                    .created(URI.create("/commandes" + commandeDto.getId()))
                    .body(commandeDtoReponse);
        } catch (UnknownClientException | EmptyArticlesException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {APPLICATION_JSON_VALUE}, consumes = {APPLICATION_JSON_VALUE})
    public ResponseEntity<CommandeDto> updateCommande(@RequestBody CommandeDto commandeDto, @PathVariable Integer id) {
        try {
            commandeDto.setId(id);
            CommandeDto commandeDtoResponse = commandeMapper.mapToDto(commandeService.updateCommande(
                    commandeMapper.mapToModel(commandeDto)
            ));
            return ResponseEntity.ok(commandeDtoResponse);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE})
    @ApiResponse(responseCode = "200", description = "Succes")
    @ApiResponse(responseCode = "400", description = "Echec")
    public ResponseEntity<CommandeDto> getCommandeById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.commandeMapper
                    .mapToDto(this.commandeService.getCommandeById(id)));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

