package com.exo.tp.exo.api;

import com.exo.tp.exo.dto.ArticleDto;
import com.exo.tp.exo.mapper.ArticleMapper;
import com.exo.tp.exo.service.ArticleService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "api/v1/articles", produces = APPLICATION_JSON_VALUE)
public class ArticleApi {

    private final ArticleService articleService;
    private final ArticleMapper articleMapper;

    public ArticleApi(ArticleService articleService, ArticleMapper articleMapper) {
        this.articleService = articleService;
        this.articleMapper = articleMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<ArticleDto>> getAllArticles() {
        return ResponseEntity.ok(
                this.articleService.getAllArticles().stream()
                        .map(this.articleMapper::mapToDto)
                        .toList()
        );
    }

    @PostMapping(
            consumes = {APPLICATION_JSON_VALUE},
            produces = {APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @ApiResponse(responseCode = "201", description = "Succes")
    public ResponseEntity<ArticleDto> createArticle(@RequestBody final ArticleDto articleDto) {
        try {
            ArticleDto articleDtoReponse = this.articleMapper.mapToDto(
                    this.articleService.createArticle(
                            this.articleMapper.mapToModel(articleDto)
                    )
            );
            return ResponseEntity
                    .created(URI.create("/articles" + articleDto.getId()))
                    .body(articleDtoReponse);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PutMapping(path = "/{id}", produces = {APPLICATION_JSON_VALUE}, consumes = {APPLICATION_JSON_VALUE})
    public ResponseEntity<ArticleDto> updateArticle(@RequestBody ArticleDto articleDto, @PathVariable Integer id) {
        try {
            articleDto.setId(id);
            ArticleDto articleDtoResponse = articleMapper.mapToDto(articleService.updateArticle(
                    articleMapper.mapToModel(articleDto)
            ));
            return ResponseEntity.ok(articleDtoResponse);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE})
    @ApiResponse(responseCode = "200", description = "Succes")
    @ApiResponse(responseCode = "400", description = "Echec")
    public ResponseEntity<ArticleDto> getArticleById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.articleMapper
                    .mapToDto(this.articleService.getArticleById(id)));
    } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
