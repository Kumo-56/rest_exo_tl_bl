package com.exo.tp.exo.api;

import com.exo.tp.exo.dto.ClientDto;
import com.exo.tp.exo.mapper.ClientMapper;
import com.exo.tp.exo.service.ClientService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api/v1/clients", produces = APPLICATION_JSON_VALUE)
public class ClientApi {

    private final ClientService clientService;
    private final ClientMapper clientMapper;

    @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE})
    @ApiResponse(responseCode = "200", description = "Succes")
    @ApiResponse(responseCode = "400", description = "Echec")

    public ResponseEntity<ClientDto> getArticleById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.clientMapper
                    .mapToDto(this.clientService.getClientById(id)));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
