package com.exo.tp.exo.exception;

public class EmptyArticlesException extends Exception{
    public EmptyArticlesException() {
        super();
    }

    public EmptyArticlesException(String message) {
        super(message);
    }
}
