package com.exo.tp.exo.exception;

public class UnknownClientException extends Exception{
    public UnknownClientException() {
    }

    public UnknownClientException(String message) {
        super(message);
    }
}
