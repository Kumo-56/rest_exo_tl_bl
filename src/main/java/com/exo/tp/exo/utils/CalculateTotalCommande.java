package com.exo.tp.exo.utils;

import com.exo.tp.exo.model.Article;

import java.util.List;

public class CalculateTotalCommande {

    private CalculateTotalCommande(){}

    public static float calculateTotal(List<Article> articles){
        return articles.stream()
                .map(Article::getPrix)
                .reduce(0f, Float::sum);
    }

}
