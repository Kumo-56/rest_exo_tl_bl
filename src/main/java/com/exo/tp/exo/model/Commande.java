package com.exo.tp.exo.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;

import java.util.List;


@AllArgsConstructor
@Entity
@Table(name = "commande")
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany(cascade = { CascadeType.ALL, CascadeType.MERGE })
    @JoinTable(
            name = "commande_Article",
            joinColumns = { @JoinColumn(name = "commande_id") },
            inverseJoinColumns = { @JoinColumn(name = "article_id") }
    )
    private List<Article> listeArticles;


    @Transient
    private float total;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "client id")
    private Client client;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Article> getListeArticles() {
        return listeArticles;
    }

    public void setListeArticles(List<Article> listeArticles) {
        this.listeArticles = listeArticles;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(float prix) {
        this.total = prix;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Commande () {}

    public Commande(int id) {
        this.id = id;
    }
}
