package com.exo.tp.exo.service;

import com.exo.tp.exo.exception.EmptyArticlesException;
import com.exo.tp.exo.exception.UnknownClientException;
import com.exo.tp.exo.model.Commande;

import java.util.List;

public interface CommandeService {

    List<Commande> getAllCommandes();

    Commande createCommande(Commande commande) throws UnknownClientException, EmptyArticlesException;

    void deleteCommande(Commande commande);

    Commande updateCommande(Commande commande);

    Commande getCommandeById(int id);
}
