package com.exo.tp.exo.service;

import com.exo.tp.exo.model.Client;

import java.util.List;

public interface ClientService {

    List<Client> getAllClients();

    Client createClient(Client article);

    void deleteClient(Client article);

    Client updateClient(Client article);

    Client getClientById(int id);
}
