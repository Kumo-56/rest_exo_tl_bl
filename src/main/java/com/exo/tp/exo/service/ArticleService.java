package com.exo.tp.exo.service;

import com.exo.tp.exo.model.Article;

import java.util.List;

public interface ArticleService {

    List<Article> getAllArticles();

    Article createArticle(Article article);

    void deleteArticle(Article article);

    Article updateArticle(Article article);

    Article getArticleById(int id);

}
