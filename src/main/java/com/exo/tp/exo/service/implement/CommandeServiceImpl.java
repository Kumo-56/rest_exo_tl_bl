package com.exo.tp.exo.service.implement;

import com.exo.tp.exo.exception.EmptyArticlesException;
import com.exo.tp.exo.exception.UnknownClientException;
import com.exo.tp.exo.model.Article;
import com.exo.tp.exo.model.Commande;
import com.exo.tp.exo.repository.CommandeRepository;
import com.exo.tp.exo.service.ArticleService;
import com.exo.tp.exo.service.ClientService;
import com.exo.tp.exo.service.CommandeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommandeServiceImpl implements CommandeService {
    Logger log = LoggerFactory.getLogger(CommandeServiceImpl.class);

    private final CommandeRepository commandeRepository;

    private final ClientService clientService;
    private final ArticleService articleService;

    @Override
    public List<Commande> getAllCommandes() {
        return commandeRepository.findAll();
    }

    @Override
    public Commande createCommande(Commande commande) throws UnknownClientException, EmptyArticlesException{
        log.info("Debut de la creation de la commande");
        if (commande.getClient()==null ){
            log.error("il n'y a pas de client dans la commande");
            throw new UnknownClientException("pas de client!!");
        }
        if (commande.getListeArticles().isEmpty()){
            log.error("il n'y a pas d'articles dans la commande");
            throw new EmptyArticlesException("pas d'articles!!");
        }
        List<Article> tmpArticles=new ArrayList<>();
        for (Article article:commande.getListeArticles()){
            Article tmpArticle=this.articleService.getArticleById(article.getId());
            if (tmpArticle.getQuantite()>0){
                tmpArticle.setQuantite(tmpArticle.getQuantite()-1);
            }else{
                log.error("il n'y a pas d'articles disponibles "+ tmpArticle.getQuantite());
                throw new EmptyArticlesException("Cet article n'est plus disponible actuellement");
            }
            tmpArticles.add(tmpArticle);
            articleService.updateArticle(tmpArticle);

        }
        commande.setListeArticles(tmpArticles);
        return commandeRepository.save(commande);
    }

    @Override
    public void deleteCommande(Commande commande) {
        commandeRepository.delete(commande);
    }

    @Override
    public Commande updateCommande(Commande commande) {
        return commandeRepository.save(commande);
    }

    @Override
    public Commande getCommandeById(int id) {
        return commandeRepository.getReferenceById(id);
    }
}
