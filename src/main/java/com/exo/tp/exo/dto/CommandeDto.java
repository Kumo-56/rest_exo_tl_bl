package com.exo.tp.exo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommandeDto {
    private int id;
    private List<ArticleDto> listeArticles;
    private float total;
    private int clientId;
    private String clientMail;
}
