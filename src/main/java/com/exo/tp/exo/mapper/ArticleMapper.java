package com.exo.tp.exo.mapper;

import com.exo.tp.exo.dto.ArticleDto;
import com.exo.tp.exo.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ArticleMapper {


    ArticleDto mapToDto(Article article);

    @Mapping(target = "listeCommandes", ignore = true)
    Article mapToModel(ArticleDto articleDto);
}
