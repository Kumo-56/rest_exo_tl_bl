package com.exo.tp.exo.mapper;


import com.exo.tp.exo.dto.ClientDto;
import com.exo.tp.exo.model.Client;
import com.exo.tp.exo.model.Commande;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface ClientMapper {

    @Mapping(target = "commandesIds", expression = "java(getcommandesIds(client))")
    ClientDto mapToDto(Client client);

    default List<Integer> getcommandesIds(Client client) {
        List<Integer> commandes = new ArrayList<>();

        if (client.getListeCommandes() != null) {
            commandes = client.getListeCommandes().stream()
                    .map(Commande::getId).toList();
        }
        return commandes;
    }

    default List<Commande> getCommandes(ClientDto clientDto) {
        List<Commande> commandes = new ArrayList<>();

        if (clientDto.getCommandesIds() != null) {
            commandes = clientDto.getCommandesIds().stream()
                    .map(Commande::new).toList();
        }
        return commandes;
    }
    @Mapping(target = "listeCommandes", expression = "java(getCommandes(clientDto))")
    Client mapToModel(ClientDto clientDto);
}
