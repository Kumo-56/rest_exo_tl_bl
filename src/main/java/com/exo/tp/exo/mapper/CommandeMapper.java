package com.exo.tp.exo.mapper;

import com.exo.tp.exo.dto.CommandeDto;
import com.exo.tp.exo.model.Commande;
import com.exo.tp.exo.utils.CalculateTotalCommande;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
@Component
@Mapper(componentModel = "spring")
public interface CommandeMapper {


    @Mapping(source = "client.id", target = "clientId")
    @Mapping(source = "client.mail", target = "clientMail")
    @Mapping(target = "total", expression = "java(com.exo.tp.exo.utils.CalculateTotalCommande.calculateTotal(commande.getListeArticles()))")
    CommandeDto mapToDto(Commande commande);

    @Mapping(source = "clientId", target = "client.id")
    @Mapping(source = "clientMail", target = "client.mail")
    Commande mapToModel(CommandeDto commandeDto);
}
