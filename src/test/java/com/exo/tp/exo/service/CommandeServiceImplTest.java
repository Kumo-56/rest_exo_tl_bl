package com.exo.tp.exo.service;

import com.exo.tp.exo.exception.EmptyArticlesException;
import com.exo.tp.exo.exception.UnknownClientException;
import com.exo.tp.exo.model.Article;
import com.exo.tp.exo.model.Client;
import com.exo.tp.exo.model.Commande;
import com.exo.tp.exo.repository.CommandeRepository;
import com.exo.tp.exo.service.implement.ArticleServiceImpl;
import com.exo.tp.exo.service.implement.CommandeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class CommandeServiceImplTest {

    @Mock
    ArticleServiceImpl articleService;

    @Mock
    CommandeRepository commandeRepository;

    @InjectMocks
    CommandeServiceImpl commandeService;

    @Test
    void createCommandeOk() throws Exception {
        Client client=new Client(1,"test",null);
        Article article=new Article(1,5, 12.5F,"test",null);
        ArrayList<Article> articles=new ArrayList<>();
        articles.add(article);
        Mockito.when(articleService.getArticleById(article.getId())).thenReturn(article);

        Commande commande=new Commande();
        commande.setClient(client);
        commande.setListeArticles(articles);

        commandeService.createCommande(commande);
        Assertions.assertEquals(4,article.getQuantite());
    }

    @Test
    void createCommandeArticleUnavailable() {
        Client client=new Client(1,"test",null);
        Article article=new Article(1,1, 12.5F,"test",null);
        ArrayList<Article> articles=new ArrayList<>();
        articles.add(article);
        articles.add(article);
        Mockito.when(articleService.getArticleById(article.getId())).thenReturn(article);

        Commande commande=new Commande();
        commande.setClient(client);
        commande.setListeArticles(articles);


        Assertions.assertThrows(EmptyArticlesException.class, () -> commandeService.createCommande(commande));
    }

    @Test
    void createCommandeWithoutClient()  {

        Article article=new Article(1,5, 12.5F,"test",null);
        ArrayList<Article> articles=new ArrayList<>();
        articles.add(article);

        Commande commande=new Commande();
        commande.setClient(null);
        commande.setListeArticles(articles);


        Assertions.assertThrows(UnknownClientException.class, () -> commandeService.createCommande(commande));


    }
}
